package sagittarius.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public abstract class EntityCircle extends Entity {

    // ---------- ATTRIBUTEs ----------

    protected float radius;

    // ---------- CONSTRUCTORs ----------

    /**
     * Creates an {@link #Entity}.
     * 
     * @param angle angle associated to the {@link #Entity}.
     * @param mass mass of the {@link #Entity}.
     * @param color color of the {@link #Entity}.
     * @param position position of the {@link #Entity}.
     */
    public EntityCircle(float angle, float mass, Color color, Vector2 position, float radius) {
        super(angle, mass, color, position);
        this.radius = radius;
    }

    // ---------- GETTERs ----------

    /**
     * @return radius of the {@link #EntityCircle}.
     */
    public float getRadius() {
        return this.radius;
    }

    // ---------- SETTERs ----------

    /**
     * Sets the radius of the {@link #EntityCircle}.
     *
     * @param radius new radius.
     */
    public void setRadius(float radius) {
        this.radius = radius;
    }
    // ---------- METHODs ----------

    @Override
    protected float[] getHitbox() { // peut être optimisé
        int segments = 64;
        float[] vertices = new float[2 * segments];

        float theta = 360.0f / segments;
        for (int i = 0; i < segments; i++) {
            vertices[2 * i] = getX() + MathUtils.cosDeg(theta * (i + 1) + getRotation()) * radius;
            vertices[2 * i + 1] =
                    getY() + MathUtils.sinDeg(theta * (i + 1) + getRotation()) * radius;
        }

        return vertices;
    }

}

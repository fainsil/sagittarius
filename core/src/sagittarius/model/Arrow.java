package sagittarius.model;

import com.badlogic.gdx.Gdx;
import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.audio.Sound;

import sagittarius.SagittariusGame;
import sagittarius.view.GameScreen;

public class Arrow extends EntityQuad {

    // ---------- ATTRIBUTEs ----------

    private Vector2 velocity;
    private Vector2 acceleration;
    private Vector2 force;

    private float TTL = 7;
    private boolean landed;

    private Planet crash;
    private Vector2 offset;

    private Sound arrowLandedSound;
    private Sound arrowHitSound;
    private ArrayList<Texture> texture;

    // ---------- CONSTRUCTORs ----------

    /**
     * Constructs an Arrow using its initial conditions.
     *
     * @param angle initial angle of the Arrow (in degrees).
     * @param power power given to the Arrow by the Bow.
     * @param shooter Bow's shooter.
     */
    Arrow(float angle, float power, Player shooter, boolean preview) {
        super(0, 1, shooter.getColor(), shooter.getPosition());
        this.velocity = new Vector2(power, 0).setAngleDeg(angle);
        this.acceleration = new Vector2();
        this.setOrigin(40, 2);
        this.setSize(50, 4);
        this.force = computeForce();
        this.landed = false;

        if (!preview) {

            arrowLandedSound =
                    Gdx.audio.newSound(Gdx.files.internal("sounds/effects/arrow_landed_trim.mp3"));
            arrowHitSound =
                    Gdx.audio.newSound(Gdx.files.internal("sounds/effects/player_death_trim.mp3"));

            texture = new ArrayList<>();
            String path = "arrow" + MathUtils.random(2);

            int i = 0;
            while (true) {
                try {
                    Pixmap pm = new Pixmap(Gdx.files.internal(path + "-" + i + ".png"));
                    pm.setBlending(Pixmap.Blending.None);
                    for (int x = 0; x < pm.getWidth(); x++) {
                        for (int y = 0; y < pm.getHeight(); y++) {

                            Color pc = new Color();
                            Color.rgba8888ToColor(pc, pm.getPixel(x, y));

                            if (pc.r == 1 && pc.g == 1 && pc.b == 1) {
                                pc.r = getColor().r;
                                pc.g = getColor().g;
                                pc.b = getColor().b;
                            }

                            pm.drawPixel(x, y, Color.rgba8888(pc));
                        }
                    }
                    texture.add(new Texture(pm));
                    i++;
                } catch (com.badlogic.gdx.utils.GdxRuntimeException e) {
                    break;
                }
            }
        }

    }

    // ---------- METHODs ----------

    @Override
    public void act(float dt) {
        super.act(dt);
        if (!landed) {
            integrationVerlet(dt);
            this.TTL -= dt;
            this.setRotation(this.velocity.angleDeg());

            verifyLanding();
            verifyHitting();

            if (TTL <= 0) {
                GameScreen.arrows.removeActor(this);
                GameScreen.nextPlayer();
                GameScreen.setFocus(GameScreen.playerCurrent);
            }
        } else {
            this.setPosition(crash.getPosition().cpy().add(offset));
        }

    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        if (!landed) {
            if (getStage() != null)
                shapes.setColor(getStage().getDebugColor());
            for (Actor actor : GameScreen.attractors.getChildren()) {
                shapes.line(getX(), getY(), actor.getX(), actor.getY());
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        int i = (int) (TTL * 3) % texture.size();
        batch.draw(texture.get(i), getPosition().x - getOriginX(), getPosition().y - getWidth() / 2,
                getOriginX(), getWidth() / 2, getWidth(), getWidth(), 1, 1, getRotation(), 0, 0,
                texture.get(i).getWidth(), texture.get(i).getHeight(), false, false);
        super.draw(batch, parentAlpha);
    }

    /**
     * Computes the {@link Arrow#force} exerted on the Arrow, according to the other weighted
     * entities.
     * 
     * @return the complete force exerted on the Arrow.
     */
    private Vector2 computeForce() {
        Vector2 force = new Vector2();
        for (Actor actor : GameScreen.attractors.getChildren()) {
            Planet attractor = (Planet) actor;
            Vector2 diff = attractor.getPosition().cpy().sub(this.getPosition());
            Vector2 attraction = diff.scl(SagittariusGame.G * attractor.mass / diff.len2());
            force.add(attraction);
        }
        return force;
    }

    /**
     * Computes the next position of the Arrow according to its physical attributes using the Verlet
     * integration scheme.
     *
     * @param dt time difference used in the integration.
     * @see <a href=
     *      "https://gamedev.stackexchange.com/a/41917">https://gamedev.stackexchange.com/a/41917</a>.
     */
    private void integrationVerlet(float dt) {

        this.acceleration = this.force.cpy();

        this.moveBy(dt * (this.velocity.x + dt * this.acceleration.x / 2),
                dt * (this.velocity.y + dt * this.acceleration.y / 2));

        this.force = computeForce();

        this.velocity.x += dt * (this.acceleration.x + this.force.x) / 2;
        this.velocity.y += dt * (this.acceleration.y + this.force.y) / 2;
    }

    /**
     * TODO
     */
    private void verifyLanding() {
        for (Actor actor : GameScreen.attractors.getChildren()) {
            Planet planet = (Planet) actor;
            if (planet.hitbox.contains(this.getPosition())) {
                landed = true;

                // Make a sound when an arrow lands
                if (!SagittariusGame.disableSounds) {
                    arrowLandedSound.play(SagittariusGame.soundsVolume);
                }

                this.crash = planet;
                this.offset = this.getPosition().sub(planet.getPosition());
                GameScreen.nextPlayer();
                GameScreen.setFocus(GameScreen.playerCurrent);
                break;
            }
        }
    }

    private boolean hasLanded() {
        for (Actor actor : GameScreen.attractors.getChildren()) {
            Planet planet = (Planet) actor;
            if (planet.hitbox.contains(this.getPosition())) {
                return true;
            }
        }
        return false;
    }

    /**
     * TODO
     */
    private void verifyHitting() {
        for (Actor actor : GameScreen.players.getChildren()) {
            Player player = (Player) actor;
            if (player == GameScreen.playerCurrent && TTL > 6.5)
                continue;
            if (Intersector.overlapConvexPolygons(player.hitbox, this.hitbox)) {

                // Make a sound when an arrow kills somebody
                if (!SagittariusGame.disableSounds) {
                    arrowHitSound.play(SagittariusGame.soundsVolume);
                }

                GameScreen.removePlayer(player);
                break;

            }
        }
    }

    private boolean hasHit() {
        for (Actor actor : GameScreen.players.getChildren()) {
            Player player = (Player) actor;
            if (player == GameScreen.playerCurrent)
                continue;
            if (player.hitbox.contains(this.getPosition())) {
                return true;
            }
        }
        return false;
    }

    /**
     * // TODO : pass directly an Arrow instead of 3 arguments
     * 
     * Computes the trajectory of an Arrow, given its initial conditions.
     *
     * @param iterations number of iterations for the integration.
     * @param timeStep time period used for the integration.
     * @return an array of vertices describing the trajectory of the Arrow.
     */
    static Trajectory computeTrajectory(float angle, float power, Player shooter, int iterations,
            float timeStep) {
        Trajectory traj = new Trajectory(iterations); // TODO: not optimal
        Arrow dummyArrow = new Arrow(angle, power, shooter, true);
        for (int i = 0; i < iterations; i++) {
            dummyArrow.integrationVerlet(timeStep);
            traj.add(dummyArrow.getPosition());
            if (dummyArrow.hasLanded() || dummyArrow.hasHit()) {
                break;
            }
        }
        return traj;
    }

    @Override
    protected String getInfo() {
        if (landed) {
            return "TTL=" + (int) TTL + "\n" + "pos=" + (int) getX() + "," + (int) getY() + "\n"
                    + "angle=" + (int) getRotation();
        } else {
            return "TTL=" + (int) TTL + "\n" + "pos=" + (int) getX() + "," + (int) getY() + "\n"
                    + "speed=" + (int) velocity.x + "," + (int) velocity.y + "\n" + "accel="
                    + (int) acceleration.x + "," + (int) acceleration.y + "\n" + "force="
                    + (int) force.x + "," + (int) force.y + "\n" + "angle=" + (int) getRotation();
        }
    }

}

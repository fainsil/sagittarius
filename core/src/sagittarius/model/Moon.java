package sagittarius.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Moon extends Planet {

    private Planet sun;
    private float altitude;

    // ---------- CONSTRUCTORs ----------

    public Moon(Planet sun, float mass, float radius, float altitude, Color color) {
        super(new Vector2(), mass, radius, color);
        this.angle = MathUtils.random(360);
        this.sun = sun;
        this.altitude = altitude;
    }

    // ---------- GETTERs ----------

    /**
     * @return the Planet which the Moon orbits.
     */
    Planet getSun() {
        return this.sun;
    }

    // ---------- METHODs ----------

    @Override
    public void act(float dt) {
        this.angle += 2000.0f / this.altitude * dt;
        this.setX(sun.getX() + this.altitude * MathUtils.cosDeg(this.angle));
        this.setY(sun.getY() + this.altitude * MathUtils.sinDeg(this.angle));
        super.act(dt);
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        if (getStage() != null)
            shapes.setColor(getStage().getDebugColor());
        shapes.line(getX(), getY(), sun.getX(), sun.getY());
    }

}

package sagittarius.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import sagittarius.view.GameScreen;

public abstract class Entity extends Actor {

    // ---------- ATTRIBUTEs ----------

    // private @Null String name;
    // private Touchable touchable = Touchable.enabled;
    // private boolean visible = true, debug;
    // float x, y;
    // float width, height;
    // float originX, originY;
    // float scaleX = 1, scaleY = 1;
    // float rotation;
    // final Color color = new Color(1, 1, 1, 1);

    protected float angle;
    protected float mass;
    protected Polygon hitbox;

    // ---------- CONSTRUCTORs ----------

    /**
     * Creates an {@link #Entity}.
     * 
     * @param angle angle associated to the {@link #Entity}.
     * @param mass mass of the {@link #Entity}.
     * @param color color of the {@link #Entity}.
     * @param position position of the {@link #Entity}.
     */
    public Entity(float angle, float mass, Color color, Vector2 position) {
        this.angle = angle;
        this.mass = mass;
        this.setColor(color);
        this.setPosition(position);
        this.hitbox = new Polygon(getHitbox());
    }

    // ---------- GETTERs ----------

    /**
     * @return angle associated to the {@link #Entity}.
     */
    public float getAngle() {
        return this.angle;
    }

    /**
     * @return mass of the {@link #Entity}.
     */
    public float getMass() {
        return this.mass;
    }

    /**
     * @return position of the {@link #Entity}.
     */
    public Vector2 getPosition() {
        return new Vector2(getX(), getY());
    }

    // ---------- SETTERs ----------

    /**
     * Sets the angle associated to the {@link #Entity}.
     * 
     * @param angle new angle.
     */
    public void setAngle(float angle) {
        this.angle = angle;
    }

    /**
     * Sets the mass of the {@link #Entity}.
     * 
     * @param mass new mass.
     */
    public void setMass(float mass) {
        this.mass = mass;
    }

    /**
     * Sets the Position of the {@link #Entity}.
     * 
     * @param position new position.
     */
    public void setPosition(Vector2 position) {
        this.setX(position.x);
        this.setY(position.y);
    }

    // ---------- METHODs ----------

    /**
     * @return String containing informations about the {@link #Entity}
     */
    protected abstract String getInfo();

    /**
     * inpired from {@link ShapeRenderer#rect}
     * 
     * @return vertices of the hitbox polygon
     */
    protected abstract float[] getHitbox();

    @Override
    public void act(float dt) {
        super.act(dt);
        hitbox.setVertices(getHitbox());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (getDebug())
            GameScreen.fontDebug.draw(batch, getInfo(), getX(), getY());
    }

    @Override
    protected void drawDebugBounds(ShapeRenderer shapes) {
        if (!getDebug())
            return;
        shapes.set(ShapeType.Line);
        shapes.setColor(this.getColor());
        shapes.polygon(hitbox.getVertices());
    }

}

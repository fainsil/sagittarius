package sagittarius.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.audio.Sound;

import sagittarius.view.GameScreen;
import sagittarius.SagittariusGame;

public class Bow extends Actor {

    // ---------- ATTRIBUTEs ----------

    private boolean aimAssist = false;
    private boolean pressed = false;
    private float angle;
    private Sound shotSound =
            Gdx.audio.newSound(Gdx.files.internal("sounds/effects/arrow_shot_trim.mp3"));
    private Vector2 anchor = new Vector2();
    private Vector2 aim = new Vector2();

    private float power;

    // ---------- CONSTRUCTORs ----------

    public Bow(boolean aimAssist) {
        super();
        this.aimAssist = aimAssist;
    }

    // ---------- METHODs ----------

    @Override
    public void act(float dt) {
        super.act(dt);
        if (GameScreen.playerCurrent.isActive()
                && Gdx.input.isButtonJustPressed(SagittariusGame.shootArrowButton) && !pressed) {
            this.anchor = GameScreen.worldCursor.cpy();
            pressed = true;
        } else if (Gdx.input.isButtonPressed(SagittariusGame.shootArrowButton) && pressed) {
            aim = this.anchor.cpy().sub(GameScreen.worldCursor);
            angle = aim.angleDeg();
            power = MathUtils.clamp(aim.len(), 0, 1000);
        } else if (pressed && power > 50) {
            pressed = false;
            GameScreen.playerCurrent.setActive(false);
            Arrow arrowShot = new Arrow(angle, power, GameScreen.playerCurrent, false);
            GameScreen.setFocus(arrowShot); // do not use constructor 2 times
            GameScreen.arrows.addActor(arrowShot);

            // Make a sound when an arrow is shot
            if (!SagittariusGame.disableSounds) {
                shotSound.play(SagittariusGame.soundsVolume);
            }
        } else {
            pressed = false;
        }
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        if (pressed) {
            if (power > 50) {
                shapes.setColor(Color.RED);
            }
            // shapes.line(this.anchor, GameScreen.worldCursor);
            if (aimAssist) {
                Trajectory traj =
                        Arrow.computeTrajectory(angle, power, GameScreen.playerCurrent, 50, 0.01f);
                if (traj.actualSize > 2) {
                    shapes.polyline(traj.path, 0, traj.actualSize);
                }
            }
        }
    }

}

package sagittarius.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Planet extends EntityCircle {

    // ---------- ATTRIBUTEs ----------

    float revolutionRate = MathUtils.randomTriangular(1) + 1;
    private Texture texture, backGround;

    // ---------- CONSTRUCTORs ----------

    public Planet(Vector2 position, float mass, float radius, Color color) {
        super(0, mass, color, position, radius);

        Pixmap pm = new Pixmap(Gdx.files.internal("planet" + MathUtils.random(4) + ".png"));

        pm.setBlending(Pixmap.Blending.None);
        for (int x = 0; x < pm.getWidth(); x++) {
            for (int y = 0; y < pm.getHeight(); y++) {

                Color pc = new Color();
                Color.rgba8888ToColor(pc, pm.getPixel(x, y));

                if (pc.r == 1 && pc.g == 1 && pc.b == 1) {
                    pc.r = color.r;
                    pc.g = color.g;
                    pc.b = color.b;
                }

                pm.drawPixel(x, y, Color.rgba8888(pc));
            }
        }
        texture = new Texture(pm);

        pm = new Pixmap(Gdx.files.internal("planet0.png"));
        pm.setBlending(Pixmap.Blending.None);
        for (int x = 0; x < pm.getWidth(); x++) {
            for (int y = 0; y < pm.getHeight(); y++) {

                Color pc = new Color();
                Color.rgba8888ToColor(pc, pm.getPixel(x, y));

                if (pc.r == 1 && pc.g == 1 && pc.b == 1) {
                    pc.r = color.r;
                    pc.g = color.g;
                    pc.b = color.b;
                    pc.a = 0.5f;
                }

                pm.drawPixel(x, y, Color.rgba8888(pc));
            }
        }
        backGround = new Texture(pm);
    }

    // ---------- METHODs ----------

    @Override
    public void act(float dt) {
        super.act(dt);
        this.rotateBy(revolutionRate / this.radius);
    }

    @Override
    protected String getInfo() {
        return "radius=" + (int) radius + "\n" + "mass=" + (int) mass + "\n" + "pos=" + (int) getX()
                + "," + (int) getY();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(backGround, getPosition().x - this.radius, getPosition().y - this.radius,
                this.radius, this.radius, 2 * this.radius, 2 * this.radius, 1, 1, getRotation(), 0,
                0, texture.getWidth(), texture.getHeight(), false, false);
        batch.draw(texture, getPosition().x - this.radius, getPosition().y - this.radius,
                this.radius, this.radius, 2 * this.radius, 2 * this.radius, 1, 1, getRotation(), 0,
                0, texture.getWidth(), texture.getHeight(), false, false);
        super.draw(batch, parentAlpha);
    }

}

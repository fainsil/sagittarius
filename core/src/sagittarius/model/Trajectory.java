package sagittarius.model;

import com.badlogic.gdx.math.Vector2;

public class Trajectory {

    float[] path;
    int actualSize;

    public Trajectory(int iterations) {
        path = new float[2 * iterations];
    }

    public void add(float x, float y) {
        path[actualSize++] = x;
        path[actualSize++] = y;
    }

    public void add(Vector2 vec) {
        path[actualSize++] = vec.x;
        path[actualSize++] = vec.y;
    }

}

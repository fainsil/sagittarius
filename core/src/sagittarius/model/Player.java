package sagittarius.model;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import sagittarius.SagittariusGame;

public class Player extends EntityQuad {

    // ---------- ATTRIBUTEs ----------

    private Planet home;
    private boolean active;
    private ArrayList<Texture> texture;
    private int T = 0;

    // ---------- CONSTRUCTORs ----------

    public Player(Planet home, Color color) {
        super(MathUtils.random(360), 0, color, home.getPosition());
        this.setRotation(home.getRotation() + angle - 90);

        this.setSize(50, 100);
        this.setOrigin(getWidth() / 2, getHeight() / 2);

        this.home = home;

        texture = new ArrayList<>();
        String path = "player0";

        int i = 0;
        while (true) {
            try {
                Pixmap pm = new Pixmap(Gdx.files.internal(path + "-" + i + ".png"));
                pm.setBlending(Pixmap.Blending.None);
                for (int x = 0; x < pm.getWidth(); x++) {
                    for (int y = 0; y < pm.getHeight(); y++) {

                        Color pc = new Color();
                        Color.rgba8888ToColor(pc, pm.getPixel(x, y));

                        if (pc.r == 1 && pc.g == 1 && pc.b == 1) {
                            pc.r = color.r;
                            pc.g = color.g;
                            pc.b = color.b;
                        }

                        pm.drawPixel(x, y, Color.rgba8888(pc));
                    }
                }
                texture.add(new Texture(pm));
                i++;
            } catch (com.badlogic.gdx.utils.GdxRuntimeException e) {
                break;
            }
        }
    }

    // ---------- METHODs ----------

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        if (getStage() != null)
            shapes.setColor(getStage().getDebugColor());
        shapes.line(home.getX(), home.getY(), getX(), getY());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        T++;
        T %= 100;
        int i = (int) (T * ((float) texture.size() / 100));

        batch.draw(texture.get(i), getPosition().x - getWidth() / 2,
                getPosition().y - getHeight() / 2, getWidth() / 2, getHeight() / 2, getWidth(),
                getHeight(), 1, 1, getRotation(), 0, 0, texture.get(i).getWidth(),
                texture.get(i).getHeight(), false, false);
        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        if (active) {
            if (Gdx.input.isKeyPressed(SagittariusGame.moveLeftKey)) {
                this.angle += 10000.0f / home.getRadius() * dt;
            }
            if (Gdx.input.isKeyPressed(SagittariusGame.moveRightKey)) {
                this.angle -= 10000.0f / home.getRadius() * dt;
            }
        }

        setX(home.getX() + (home.getRadius() + getHeight() / 2)
                * MathUtils.cosDeg(home.getRotation() + angle));
        setY(home.getY() + (home.getRadius() + getHeight() / 2)
                * MathUtils.sinDeg(home.getRotation() + angle));

        this.setRotation(home.getRotation() + angle - 90);
    }

    @Override
    protected String getInfo() {
        return "pos=" + (int) getX() + "," + (int) getY();
    }

    /**
     * @return the Player's home Planet.
     */
    public Planet getHome() {
        return this.home;
    }

    /**
     * Change the active state of the {@link Player}.
     * 
     * @param bool true or false
     */
    public void setActive(boolean bool) {
        active = bool;
    }

    /**
     * @return the active state of the {@link Player}.
     */
    public boolean isActive() {
        return active;
    }

}

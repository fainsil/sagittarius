package sagittarius.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public abstract class EntityQuad extends Entity {

    // ---------- CONSTRUCTORs ----------

    /**
     * Creates an {@link #Entity}.
     * 
     * @param angle angle associated to the {@link #Entity}.
     * @param mass mass of the {@link #Entity}.
     * @param color color of the {@link #Entity}.
     * @param position position of the {@link #Entity}.
     */
    public EntityQuad(float angle, float mass, Color color, Vector2 position) {
        super(angle, mass, color, position);
    }

    // ---------- METHODs ----------

    @Override
    protected float[] getHitbox() {
        float cos = MathUtils.cosDeg(getRotation());
        float sin = MathUtils.sinDeg(getRotation());
        float fx = -getOriginX();
        float fy = -getOriginY();
        float fx2 = getWidth() - getOriginX();
        float fy2 = getHeight() - getOriginY();

        if (getScaleX() != 1 || getScaleX() != 1) {
            fx *= getScaleX();
            fy *= getScaleY();
            fx2 *= getScaleX();
            fy2 *= getScaleY();
        }

        float worldOriginX = getX();
        float worldOriginY = getY();

        float x1 = cos * fx - sin * fy + worldOriginX;
        float y1 = sin * fx + cos * fy + worldOriginY;

        float x2 = cos * fx2 - sin * fy + worldOriginX;
        float y2 = sin * fx2 + cos * fy + worldOriginY;

        float x3 = cos * fx2 - sin * fy2 + worldOriginX;
        float y3 = sin * fx2 + cos * fy2 + worldOriginY;

        float x4 = x1 + (x3 - x2);
        float y4 = y3 - (y2 - y1);

        return new float[] {x1, y1, x2, y2, x3, y3, x4, y4};
    }

}

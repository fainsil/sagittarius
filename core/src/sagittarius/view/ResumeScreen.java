package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.spinner.SimpleFloatSpinnerModel;
import sagittarius.view.ButtonSounded.*;


import sagittarius.SagittariusGame;

public class ResumeScreen extends BaseScreen {

    private GameScreen gameScreen;

    public ResumeScreen(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    @Override
    public void initialize() {

        if (!SagittariusGame.disableMusic) {
            SagittariusGame.music.stop();
            SagittariusGame.music =
                    Gdx.audio.newMusic(Gdx.files.internal("sounds/music/resumeMenu_music.mp3"));
            SagittariusGame.music.setLooping(true);
            SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
            SagittariusGame.music.play();
        }
        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

        // disable music checkbox
        CheckBoxSounded disableMusic = new CheckBoxSounded("disable music");
        disableMusic.setChecked(SagittariusGame.disableMusic);

        // Change volume of the music
        SpinnerSounded musicVolume = new SpinnerSounded("Music Volume:",
                new SimpleFloatSpinnerModel(SagittariusGame.music.getVolume(), 0f, 1.0f, 0.01f, 3));

        // disable sound checkbox
        CheckBoxSounded disableSounds = new CheckBoxSounded("disable sounds");
        disableSounds.setChecked(SagittariusGame.disableSounds);

        // Change volume of the sound
        SpinnerSounded musicSounds = new SpinnerSounded("Sounds Volume:",
                new SimpleFloatSpinnerModel(SagittariusGame.soundsVolume, 0f, 1.0f, 0.01f, 3));

        // save button
        ButtonTextSounded applyButton = new ButtonTextSounded("Apply");
        applyButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                SagittariusGame.music
                        .setVolume(Float.parseFloat(musicVolume.getTextField().getText()));
                SagittariusGame.musicVolume =
                        Float.parseFloat(musicVolume.getTextField().getText());
                SagittariusGame.soundsVolume =
                        Float.parseFloat(musicSounds.getTextField().getText());

                // Deactivate music or not
                if (disableMusic.isChecked() && SagittariusGame.music.isPlaying()) {
                    SagittariusGame.music.stop();
                    SagittariusGame.disableMusic = true;
                } else if (!disableMusic.isChecked() && !SagittariusGame.music.isPlaying()) {
                    SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                    SagittariusGame.music.play();
                    SagittariusGame.disableMusic = false;
                }

                // Deactivate music or not
                if (disableSounds.isChecked()) {
                    SagittariusGame.disableSounds = true;
                }

                else {
                    SagittariusGame.disableSounds = false;
                }

            }
        });

        // Resume button
        ButtonTextSounded resumeButton = new ButtonTextSounded("Resume");
        resumeButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                if (!SagittariusGame.disableMusic) {
                    SagittariusGame.music.stop();
                    SagittariusGame.music =
                            Gdx.audio.newMusic(Gdx.files.internal("sounds/music/game_music.mp3"));
                    SagittariusGame.music.setLooping(true);
                    SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                    SagittariusGame.music.play();
                }
                SagittariusGame.setActiveScreen(gameScreen);
            }

        });

        // go back to main menu
        ButtonTextSounded returnMain = new ButtonTextSounded("Main Menu");
        returnMain.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new StartScreen());
            }
        });

        // Table structure
        table.add(resumeButton).width(250);
        table.row();
        table.add(disableMusic).width(250);
        table.row();
        table.add(musicVolume).width(250);
        table.row();
        table.add(disableSounds).width(250);
        table.row();
        table.add(musicSounds).width(250);
        table.row();
        table.add(applyButton).width(250);
        table.row();
        table.add(returnMain).width(250);
    }

    @Override
    public void update(float dt) {}

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

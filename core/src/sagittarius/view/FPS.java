package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class FPS extends Actor {

    // ---------- ATTRIBUTEs ----------

    private int frameRate;

    // ---------- METHODs ----------

    public FPS(Stage stage) {
        super();
        setPosition(3, stage.getViewport().getWorldHeight() - 3);
    }

    @Override
    public void act(float dt) {
        super.act(dt);
        frameRate = Gdx.graphics.getFramesPerSecond();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (getDebug())
            GameScreen.fontDebug.draw(batch, getInfo(), getX(), getY());
    }

    protected String getInfo() {
        return frameRate + " fps";
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import sagittarius.view.ButtonSounded.ButtonTextSounded;

import sagittarius.SagittariusGame;

public class StartScreen extends BaseScreen {

    @Override
    public void initialize() {

        // A welcome music is played
        if (!SagittariusGame.disableMusic) {
            if (SagittariusGame.music != null) {
                SagittariusGame.music.stop();
                SagittariusGame.music =
                        Gdx.audio.newMusic(Gdx.files.internal("sounds/music/mainMenu_music.mp3"));
                SagittariusGame.music.setLooping(true);
                SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                SagittariusGame.music.play();

            }

            else {
                SagittariusGame.music =
                        Gdx.audio.newMusic(Gdx.files.internal("sounds/music/mainMenu_music.mp3"));
                SagittariusGame.music.setLooping(true);
                SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                SagittariusGame.music.play();
            }

        }

        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

        // quick Button
        ButtonTextSounded playButton = new ButtonTextSounded("Play");
        playButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new GameScreen());
            }
        });

        ButtonTextSounded settingsButton = new ButtonTextSounded("Settings");
        settingsButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new SettingsScreen());
            }
        });

        ButtonTextSounded creditButton = new ButtonTextSounded("Credits");
        creditButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new CreditScreen());
            }
        });


        // Quit button
        ButtonTextSounded quitButton = new ButtonTextSounded("Exit");
        quitButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                Gdx.app.exit();
            }
        });

        // Table structure
        table.add(playButton).width(150);
        table.row();
        table.add(settingsButton).width(150);
        table.row();
        table.add(creditButton).width(150);
        table.row();
        table.add(quitButton).width(150);
        table.row();
    }

    @Override
    public void update(float dt) {
        // nothing (?)
    }

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

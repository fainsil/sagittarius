package sagittarius.view.ButtonSounded;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.audio.Sound;

import sagittarius.SagittariusGame;

public class ButtonTextSounded extends com.kotcrab.vis.ui.widget.VisTextButton {

    private Sound buttonclicked =
            Gdx.audio.newSound(Gdx.files.internal("sounds/effects/clickbutton_trim.mp3"));

    public ButtonTextSounded(String name) {
        super(name);

        this.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                if (!SagittariusGame.disableSounds) {
                    buttonclicked.play(SagittariusGame.soundsVolume);
                }
            }
        });
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MouseInfo extends Actor {

    // ---------- METHODs ----------

    @Override
    public void act(float dt) {
        super.act(dt);
        setX(GameScreen.screenCursor.x);
        setY(Gdx.graphics.getHeight() - GameScreen.screenCursor.y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (getDebug())
            GameScreen.fontDebug.draw(batch, getInfo(), getX(), getY());
    }

    protected String getInfo() {
        return "screen=" + (int) GameScreen.screenCursor.x + "," + (int) GameScreen.screenCursor.y
                + "\n" + "world=" + (int) GameScreen.worldCursor.x + ","
                + (int) GameScreen.worldCursor.y;
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;


public class SetupScreen extends BaseScreen {

    @Override
    public void initialize() {

        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisUI.load();
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

    }

    @Override
    public void update(float dt) {
        //
    }

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.badlogic.gdx.Screen;

public abstract class BaseScreen implements Screen {

    protected Stage mainStage;
    protected Stage uiStage;

    public BaseScreen() {
        mainStage = new Stage();
        mainStage.getRoot().setTransform(false);
        uiStage = new Stage();
        uiStage.getRoot().setTransform(false);
        initialize();
    }

    public abstract void initialize();

    public abstract void update(float dt);

    @Override
    public void render(float dt) {

        // update
        update(dt);

        // update Stages
        uiStage.act(dt);
        mainStage.act(dt);

        // clear screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        // draw actors on screnn
        mainStage.draw();
        uiStage.draw();
    }

    @Override
    public void dispose() {
        uiStage.dispose();
        mainStage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        mainStage.getViewport().update(width, height, false);
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void show() {}

    @Override
    public void hide() {}

}

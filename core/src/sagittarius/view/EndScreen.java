package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import sagittarius.view.ButtonSounded.*;


import sagittarius.SagittariusGame;

public class EndScreen extends BaseScreen {

    @Override
    public void initialize() {

        if (!SagittariusGame.disableMusic) {
            SagittariusGame.music.stop();
            SagittariusGame.music =
                    Gdx.audio.newMusic(Gdx.files.internal("sounds/music/resumeMenu_music.mp3"));
            SagittariusGame.music.setLooping(true);
            SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
            SagittariusGame.music.play();
        }
        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

        // Winner display 
        VisTextButton winnerDisplay = new VisTextButton("Game Finished");

        // Restart button
        ButtonTextSounded restartButton = new ButtonTextSounded("Restart");
        restartButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                if (!SagittariusGame.disableMusic) {
                    SagittariusGame.music.stop();
                    SagittariusGame.music =
                            Gdx.audio.newMusic(Gdx.files.internal("sounds/music/game_music.mp3"));
                    SagittariusGame.music.setLooping(true);
                    SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                    SagittariusGame.music.play();
                }
                SagittariusGame.setActiveScreen(new GameScreen());
            }

        });

        // go back to main menu
        ButtonTextSounded returnMain = new ButtonTextSounded("Main Menu");
        returnMain.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new StartScreen());
            }
        });

        // Table structure
        table.add(winnerDisplay).width(250);
        table.row();
        table.add(restartButton).width(250);
        table.row();
        table.add(returnMain).width(250);
    }

    @Override
    public void update(float dt) {}

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

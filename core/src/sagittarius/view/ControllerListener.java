package sagittarius.view;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kotcrab.vis.ui.widget.VisTextButton;

public class ControllerListener extends InputListener {
    private boolean capturing = false;
    private VisTextButton button;
    private VisTextButton[] other_buttons;
    private Stage uiStage;

    public ControllerListener(VisTextButton button, VisTextButton[] other_buttons, Stage uiStage) {
        super();
        this.button = button;
        this.other_buttons = other_buttons;
        this.uiStage = uiStage;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (!this.button.isDisabled() && !capturing && button == Buttons.LEFT) {
            capturing = true;
            this.button.setText("capturing...");
            for (VisTextButton other : other_buttons) {
                if (other != this.button) {
                    other.setDisabled(true);
                }
            }
            this.uiStage.setKeyboardFocus(this.button);
        }
        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        if (!this.button.isDisabled() && capturing) {
            capturing = false;
            this.button.setText(Keys.toString(keycode));
            for (VisTextButton other : other_buttons) {
                if (other != this.button) {
                    other.setDisabled(false);
                }
            }
            this.uiStage.setKeyboardFocus(null);
        }
        return true;
    }
}

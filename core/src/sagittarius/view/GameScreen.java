package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import sagittarius.SagittariusGame;
import sagittarius.model.*;

public class GameScreen extends BaseScreen implements InputProcessor {

    // ---------- GENERATION ----------
    
    public static int minDistance = 600;
    public static int maxDistance = 900;
    public static int minMass = 500;
    public static int maxMass = 1000;
    public static int minRadius = 20;
    public static int maxRadius = 150;



    // ---------- ATTRIBUTEs ----------

    public static BitmapFont fontDebug = new BitmapFont();

    // Cursors
    public static Vector2 screenCursor;
    private static Vector3 unprojectedCursor;
    public static Vector2 worldCursor;

    // Groups
    public static Group attractors;
    public static Group arrows;
    public static Group players; // TODO: move this in SagittariusGame ?

    // turn system stuff
    public static int playerIndex;
    public static Player playerCurrent;

    // camera stuff
    private Vector3 mainCameraPosition;
    private final float speed = 0.05f;
    private final float ispeed = 1.0f - speed;
    private static Entity focus;

    // test
    private OrthographicCamera gameCam;

    // ---------- METHODs ----------

    @Override
    public void initialize() {

        if (SagittariusGame.music != null) {
            SagittariusGame.music.stop();
        }        SagittariusGame.music =
                Gdx.audio.newMusic(Gdx.files.internal("sounds/music/game_music.mp3"));
        SagittariusGame.music.setLooping(true);
        SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
        if (!SagittariusGame.disableMusic) {
            SagittariusGame.music.play();
        }

        Gdx.input.setInputProcessor(this);

        // The one and only Bow
        Bow bow = new Bow(true);
        bow.setDebug(true);
        mainStage.addActor(bow);

        // arrows
        arrows = new Group();
        mainStage.addActor(arrows);

        // planets & moons
        attractors = new Group();

        for (int i = 0; i < SagittariusGame.numberPlanets; i++) {
            // Position of the new planet
            Vector2 position;
            if (i == 0) {
                position = new Vector2(0, 0);
            } else {
                int distance = minDistance + MathUtils.random(maxDistance - minDistance);
                float angle =  2*MathUtils.PI*MathUtils.random();
                float shortestDistance;
                do {
                    int index = MathUtils.random(i-1);
                    Actor planet0 = attractors.getChild(index);
                    position = new Vector2(distance * (float) Math.cos(angle), distance * (float) Math.sin(angle));
                    position.add(planet0.getX(), planet0.getY());

                    shortestDistance = maxDistance;
                    for (int j = 0; j < i-1; j++) {
                        Actor planet1 = attractors.getChild(j);
                        float dx = planet1.getX() - position.x;
                        float dy = planet1.getY() - position.y;
                        float distanceToPlanet1 = (float) Math.sqrt(dx*dx + dy*dy);
                        if (distanceToPlanet1 < shortestDistance) {
                            shortestDistance = distanceToPlanet1;
                        }
                    }
                } while (shortestDistance < minDistance);
            }

            // other attributs
            int mass = minMass + MathUtils.random(maxMass - minMass);
            int radius = minRadius + MathUtils.random(maxRadius - minRadius);
            Color color = new Color((float) Math.random(), (float) Math.random(), (float) Math.random(), 1);

            // create the planet
            Planet planet = new Planet(position, mass, radius, color);
            attractors.addActor(planet);
        }

        // create a moon
        Moon moon = new Moon(
            (Planet) attractors.getChild( MathUtils.random(SagittariusGame.numberPlanets-1)),
            minMass / 2 + MathUtils.random(maxMass - minMass) / 2,
            minRadius / 2 + MathUtils.random(maxRadius - minRadius) / 2,
            minDistance / 2,
            new Color((float) Math.random(), (float) Math.random(), (float) Math.random(), 1)
        );
        attractors.addActor(moon);

        mainStage.addActor(attractors);

        // players
        players = new Group();

        for (int i = 0; i < SagittariusGame.numberPlayers; i++) {
            Player player = new Player((Planet) attractors.getChild(i), new Color((float) Math.random(), (float) Math.random(), (float) Math.random(), 1));
            players.addActor(player);
        }

        mainStage.addActor(players);

        // others
        FPS fpsCounter = new FPS(uiStage);
        uiStage.addActor(fpsCounter);

        MouseInfo mouseInfo = new MouseInfo();
        uiStage.addActor(mouseInfo);

        mainStage.setDebugAll(SagittariusGame.debugMode);
        uiStage.setDebugAll(SagittariusGame.debugMode);

        // game turns
        playerIndex = 0;
        ((Player) players.getChild(0)).setActive(true);
        playerCurrent = (Player) players.getChild(0);

        // camera stuff
        mainCameraPosition = mainStage.getCamera().position;
        focus = playerCurrent;
        gameCam = ((OrthographicCamera) mainStage.getCamera());

    }

    @Override
    public void update(float dt) {

        screenCursor = new Vector2(Gdx.input.getX(), Gdx.input.getY());
        unprojectedCursor = mainStage.getCamera().unproject(new Vector3(screenCursor, 0));
        worldCursor = new Vector2(unprojectedCursor.x, unprojectedCursor.y);

        if (players.getChildren().size <= 1) {
            SagittariusGame.setActiveScreen(new EndScreen());
        }

        // camera zoom using keys
        if (Gdx.input.isKeyPressed(SagittariusGame.zoomOutKey)) {
            gameCam.zoom += dt;
        }
        if (Gdx.input.isKeyPressed(SagittariusGame.zoomInKey)) {
            gameCam.zoom -= dt;
        }
        // clamp zoom
        gameCam.zoom = MathUtils.clamp(gameCam.zoom, 1f, 5f);

        // Pause Menu
        if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
            SagittariusGame.setActiveScreen(new ResumeScreen(this));
        }

        // camera follow focus
        mainCameraPosition.scl(ispeed);
        mainCameraPosition.add(new Vector3(focus.getPosition(), 0).scl(speed));
    }

    /**
     * Removes a Player from the Game.
     * 
     * @param player Player to remove from the active list.
     */
    public static void removePlayer(Player player) {
        player.setActive(false);
        int index = players.getChildren().indexOf(player, true);
        if (index < playerIndex) {
            players.removeActor(player);
            playerIndex++;
            playerIndex %= players.getChildren().size;
        } else if (index == playerIndex) {
            players.removeActor(player);
            playerIndex %= players.getChildren().size;
            playerCurrent = (Player) players.getChild(playerIndex);
            playerCurrent.setActive(true);
        } else {
            players.removeActor(player);
        }
    }

    /**
     * Used to allows the next Player in the list to play.
     */
    public static void nextPlayer() {
        playerCurrent.setActive(false);
        playerIndex++;
        playerIndex %= players.getChildren().size;
        playerCurrent = (Player) players.getChild(playerIndex);
        playerCurrent.setActive(true);
    }

    public static void setFocus(Entity entity) {
        focus = entity;
    }

    // ---------- InputProcessor METHODs ----------

    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        gameCam.zoom += 2 * amountY * Gdx.graphics.getDeltaTime();
        return false;
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import sagittarius.view.ButtonSounded.*;

import sagittarius.SagittariusGame;

public class CreditScreen extends BaseScreen {

    @Override
    public void initialize() {

        // A music is played
        if (!SagittariusGame.disableMusic) {

            SagittariusGame.music.stop();
            SagittariusGame.music =
                    Gdx.audio.newMusic(Gdx.files.internal("sounds/music/credit_music.mp3"));
            SagittariusGame.music.setLooping(true);
            SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
            SagittariusGame.music.play();

        }

        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

        VisTextButton laurent = new VisTextButton("Laurent Fainsin");
        VisTextButton damien = new VisTextButton("Damien Guillotin");
        VisTextButton tom = new VisTextButton("Tom Heurtebise");
        VisTextButton pierre = new VisTextButton("Pierre-Eliot Jourdan");
        VisTextButton joan = new VisTextButton("Johan Guillemin");


        // go back button
        ButtonTextSounded returnButton = new ButtonTextSounded("Go Back");
        returnButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new StartScreen());
            }
        });

        table.add(laurent).width(300);
        table.row();
        table.add(damien).width(300);
        table.row();
        table.add(tom).width(300);
        table.row();
        table.add(pierre).width(300);
        table.row();
        table.add(joan).width(300);
        table.row();
        table.add(returnButton).width(150);
        table.row();
    }

    @Override
    public void update(float dt) {
        // nothing (?)
    }

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

package sagittarius.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.spinner.SimpleFloatSpinnerModel;
import sagittarius.view.ButtonSounded.*;

import sagittarius.SagittariusGame;

public class SettingsScreen extends BaseScreen {

    @Override
    public void initialize() {

        if (SagittariusGame.music != null) {
            SagittariusGame.music.stop();
        }
        SagittariusGame.music =
                Gdx.audio.newMusic(Gdx.files.internal("sounds/music/resumeMenu_music.mp3"));
        SagittariusGame.music.setLooping(true);
        SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
        if (!SagittariusGame.disableMusic) {
            SagittariusGame.music.play();
        }

        Gdx.input.setInputProcessor(uiStage);

        // Table creation
        VisTable table = new VisTable(true);
        table.setFillParent(true);
        uiStage.addActor(table);

        // G constant field
        SpinnerSounded gConstField = new SpinnerSounded("G constant:",
                new SimpleFloatSpinnerModel(SagittariusGame.G, 1f, 500f, 0.5f, 3));

        // number players field
        SpinnerSounded nbPlayersField = new SpinnerSounded("Number players:",
                new SimpleFloatSpinnerModel(SagittariusGame.numberPlayers, 2, 5, 1, 1));

        // number players field
        SpinnerSounded nbPlanetsField = new SpinnerSounded("Number planets:",
                new SimpleFloatSpinnerModel(SagittariusGame.numberPlanets, 4, 10, 1, 1));

        // dubug mode checkbox
        CheckBoxSounded debugModeBox = new CheckBoxSounded("debug mode");
        debugModeBox.setChecked(SagittariusGame.debugMode);

        // disable music checkbox
        CheckBoxSounded disableMusic = new CheckBoxSounded("disable music");
        disableMusic.setChecked(SagittariusGame.disableMusic);

        // Change volume of the music
        SpinnerSounded musicVolume = new SpinnerSounded("Music Volume:",
                new SimpleFloatSpinnerModel(SagittariusGame.music.getVolume(), 0f, 1.0f, 0.01f, 3));

        // disable sound checkbox
        CheckBoxSounded disableSounds = new CheckBoxSounded("disable sounds");
        disableSounds.setChecked(SagittariusGame.disableSounds);

        // Change volume of the sound
        SpinnerSounded musicSounds = new SpinnerSounded("Sounds Volume:",
                new SimpleFloatSpinnerModel(SagittariusGame.soundsVolume, 0f, 1.0f, 0.01f, 3));

        // go back button
        ButtonTextSounded returnButton = new ButtonTextSounded("Go Back");
        returnButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                SagittariusGame.setActiveScreen(new StartScreen());
            }
        });

        // Control buttons
        VisTextButton shootArrow =
                new VisTextButton(String.valueOf(SagittariusGame.shootArrowButton));;
        VisTextButton moveLeft = new VisTextButton(Keys.toString(SagittariusGame.moveLeftKey));
        VisTextButton moveRight = new VisTextButton(Keys.toString(SagittariusGame.moveRightKey));
        VisTextButton zoomIn = new VisTextButton(Keys.toString(SagittariusGame.zoomInKey));
        VisTextButton zoomOut = new VisTextButton(Keys.toString(SagittariusGame.zoomOutKey));
        VisTextButton[] other_buttons = {shootArrow, moveLeft, moveRight, zoomIn, zoomOut};

        shootArrow.addListener(new ClickListener() {
            private boolean capturing = false;

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!shootArrow.isDisabled() && !capturing && button == Buttons.LEFT) {
                    capturing = true;
                    shootArrow.setText("capturing...");
                    moveLeft.setDisabled(true);
                    moveRight.setDisabled(true);
                    zoomIn.setDisabled(true);
                    zoomOut.setDisabled(true);
                } else if (!shootArrow.isDisabled() && capturing) {
                    capturing = false;
                    shootArrow.setText(String.valueOf(button));
                    moveLeft.setDisabled(false);
                    moveRight.setDisabled(false);
                    zoomIn.setDisabled(false);
                    zoomOut.setDisabled(false);
                }
                return super.touchDown(event, x, y, pointer, button);
            };
        });
        moveLeft.addListener(new ControllerListener(moveLeft, other_buttons, uiStage));
        moveRight.addListener(new ControllerListener(moveRight, other_buttons, uiStage));
        zoomIn.addListener(new ControllerListener(zoomIn, other_buttons, uiStage));
        zoomOut.addListener(new ControllerListener(zoomOut, other_buttons, uiStage));

        // save button
        ButtonTextSounded saveButton = new ButtonTextSounded("Save");
        saveButton.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

                SagittariusGame.G = Float.parseFloat(gConstField.getTextField().getText());
                SagittariusGame.numberPlayers = (int) Float.parseFloat(nbPlayersField.getTextField().getText());
                SagittariusGame.numberPlanets = (int) Float.parseFloat(nbPlanetsField.getTextField().getText());
                SagittariusGame.music
                        .setVolume(Float.parseFloat(musicVolume.getTextField().getText()));
                SagittariusGame.musicVolume =
                        Float.parseFloat(musicVolume.getTextField().getText());
                SagittariusGame.soundsVolume =
                        Float.parseFloat(musicSounds.getTextField().getText());
                SagittariusGame.debugMode = debugModeBox.isChecked();

                SagittariusGame.moveLeftKey = Keys.valueOf(moveLeft.getText().toString());
                SagittariusGame.moveRightKey = Keys.valueOf(moveRight.getText().toString());
                SagittariusGame.zoomInKey = Keys.valueOf(zoomIn.getText().toString());
                SagittariusGame.zoomOutKey = Keys.valueOf(zoomOut.getText().toString());
                SagittariusGame.shootArrowButton =
                        Integer.parseInt(shootArrow.getText().toString());
                // Deactivate music or not
                if (disableMusic.isChecked() && SagittariusGame.music.isPlaying()) {
                    SagittariusGame.music.stop();
                    SagittariusGame.disableMusic = true;
                } else if (!disableMusic.isChecked() && !SagittariusGame.music.isPlaying()) {
                    SagittariusGame.music = Gdx.audio
                            .newMusic(Gdx.files.internal("sounds/music/resumeMenu_music.mp3"));
                    SagittariusGame.music.setVolume(SagittariusGame.musicVolume);
                    SagittariusGame.music.play();
                    SagittariusGame.disableMusic = false;
                }

                // Deactivate music or not
                if (disableSounds.isChecked()) {
                    SagittariusGame.disableSounds = true;
                } else {
                    SagittariusGame.disableSounds = false;
                }
            }
        });

        // Table structure
        table.add(gConstField).width(250).colspan(2);
        table.row();
        table.add(nbPlayersField).width(250).colspan(2);
        table.row();
        table.add(nbPlanetsField).width(250).colspan(2);
        table.row();
        table.add(disableMusic).align(Align.left).colspan(2);
        table.row();
        table.add(musicVolume).width(250).colspan(2);
        table.row();
        table.add(disableSounds).align(Align.left).colspan(2);
        table.row();
        table.add(musicSounds).width(250).colspan(2);
        table.row();
        table.add(debugModeBox).align(Align.left).colspan(2);
        table.row();
        table.add(new VisLabel("shoot")).align(Align.left);
        table.add(shootArrow).fill();
        table.row();
        table.add(new VisLabel("move left")).align(Align.left);
        table.add(moveLeft).fill();
        table.row();
        table.add(new VisLabel("move right")).align(Align.left);
        table.add(moveRight).fill();
        table.row();
        table.add(new VisLabel("zoom in")).align(Align.left);
        table.add(zoomIn).fill();
        table.row();
        table.add(new VisLabel("zoom out")).align(Align.left);
        table.add(zoomOut).fill();
        table.row();
        table.add(saveButton).width(250).colspan(2);
        table.row();
        table.add(returnButton).width(250).colspan(2);
    }

    @Override
    public void update(float dt) {}

    @Override
    public void dispose() {
        VisUI.dispose();
        super.dispose();
    }

}

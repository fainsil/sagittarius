package sagittarius;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.kotcrab.vis.ui.VisUI;
import com.badlogic.gdx.audio.Music;
import sagittarius.view.*;

public class SagittariusGame extends Game {

    // Constants
    public static float G = 100;
    public static int numberPlayers = 3;
    public static int numberPlanets = 4;
    public static boolean debugMode = false;
    public static Music music;
    public static boolean disableMusic = true;
    public static float musicVolume = 0.1f;
    public static boolean disableSounds = false;
    public static float soundsVolume = 0.1f;
    public static int moveLeftKey = Keys.LEFT;
    public static int moveRightKey = Keys.RIGHT;
    public static int zoomInKey = Keys.UP;
    public static int zoomOutKey = Keys.DOWN;
    public static int shootArrowButton = Buttons.LEFT;

    private static Game game;

    public SagittariusGame() {
        game = this;
    }

    public static void setActiveScreen(BaseScreen s) {
        game.setScreen(s);
    }

    public static BaseScreen getActiveScreen() {
        return (BaseScreen) game.getScreen();
    }

    @Override
    public void create() {

        // We load the VisUI library
        VisUI.load();

        // we set the Screen to the main menu
        game.setScreen(new StartScreen());
    }

}

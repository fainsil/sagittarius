# Sagittarius
[license-shield]: https://img.shields.io/badge/LICENSE-MIT-green?style=for-the-badge
[license-url]: https://git.inpt.fr/tobgang/sagittarius/-/blob/master/LICENSE
[![MIT License][license-shield]][license-url]

A turn based game in which players kill each others using bows, arrows and GRAVITY !

## Built with
* [openJDK 8](https://openjdk.java.net/projects/jdk/8/)
* [libGDX](https://libgdx.com/)

## Usage
Gradle should manage every dependencies for you. \
It is recommended to use [Visual Studio Code](https://code.visualstudio.com/) with this [java extension pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack).

To run the game:
```bash
./gradlew desktop:run
```
or just press (Ctrl +) F5 in vscode.

To export the game to a .jar file:
```bash
./gradlew desktop:dist
```
The resulting .jar file should be in `desktop/build/libs/`

## TODO LIST
* generate random map (with parameters)
* move freely camera
* particules de flèche, quand un joueur meurt...
* astéroïdes
* bonus/gadgets
* améliorer trajectoires arrows en simulant tout l'univers

## Contributing
Please use [conventionnal commits](https://www.conventionalcommits.org/).

## License
Distributed under the [MIT](https://choosealicense.com/licenses/mit/) License. See `LICENSE` for more information.

## Acknowledgements
* [George Prosser](https://gprosser.itch.io/sagittarius)

package sagittarius.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import sagittarius.SagittariusGame;

public class DesktopLauncher {
	public static void main(String[] arg) {

		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.useVsync(true);
		config.setForegroundFPS(120);
		config.setWindowedMode(1920, 1080);
		config.setDecorated(false);
		config.setResizable(false);

		new Lwjgl3Application(new SagittariusGame(), config);

	}
}
